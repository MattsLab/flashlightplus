package io.FallenYouth.listeners;

import io.FallenYouth.main.FlashlightPlus;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Made by FallenYouth
 */

@SuppressWarnings("deprecation")
public class EventListener implements Listener {
    private FlashlightPlus plugin;

    public EventListener(FlashlightPlus plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = (Player) e.getPlayer();
        if (plugin.getConfig().getBoolean("Backend.JoinMessage", true)) {
            player.sendMessage(ChatColor.GREEN + "This server is using FlashlightPlus made by FallenYouth");
            player.sendMessage(ChatColor.GOLD + "http://www.spigotmc.org/resources/flashlightplus.262/");
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        Player player = (Player) e.getPlayer();

        if (plugin.flashlightEnabled.contains(player.getName())) {
            plugin.flashlightEnabled.remove(player.getName());
            e.getPlayer().removePotionEffect(PotionEffectType.NIGHT_VISION);
        }
    }
   /* @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_AIR) {

            if (event.getItem().getType() == Material.TORCH) {
                Player player = event.getPlayer();

                if (!player.hasPermission("flashlight.torch.use"))
                    return;

                if (!plugin.flashlightEnabled.contains(player.getName())) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, true));
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Messages.FlashlightOnMsg")));
                    plugin.flashlightEnabled.add(player.getName());
                    player.playEffect(player.getLocation(), Effect.GHAST_SHOOT, 5);

                } else if (player.isSneaking()) {
                    player.removePotionEffect(PotionEffectType.NIGHT_VISION);
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Messages.FlashlightOffMsg")));
                    plugin.flashlightEnabled.remove(player.getName());
                    player.playEffect(player.getLocation(), Effect.EXTINGUISH, 5);
                }
            }
        }
    } */
}