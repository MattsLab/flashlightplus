package io.FallenYouth.listeners;

import io.FallenYouth.main.FlashlightPlus;
import io.FallenYouth.utils.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Made by FallenYouth
 */

public class CommandExecute implements CommandExecutor {
    public FlashlightPlus plugin;

    public String prefix = ChatColor.RED + "[FlashlightPlus]";

    public CommandExecute(FlashlightPlus plugin) {
        this.plugin = plugin;
    }

    @Override
    @SuppressWarnings("deprecation")
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Silly Console, you must be a player to use this command!");
            return true;
        }

        if (!sender.hasPermission("flashlight.use")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use the flashlight!");
            return true;
        }
        if (cmd.getName().equalsIgnoreCase("Flashlight")) {
            Player player = (Player) sender;

            if (!plugin.flashlightEnabled.contains(player.getName())) {
                player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, true));
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Messages.FlashlightOnMsg")));
                plugin.flashlightEnabled.add(player.getName());
                player.playEffect(player.getLocation(), Effect.GHAST_SHOOT, 5);
            } else {
                player.removePotionEffect(PotionEffectType.NIGHT_VISION);
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Messages.FlashlightOffMsg")));
                plugin.flashlightEnabled.remove(player.getName());
                player.playEffect(player.getLocation(), Effect.EXTINGUISH, 5);
            }

        } else if (cmd.getName().equalsIgnoreCase("Torch")) {
            Player player = (Player) sender;
            if (sender.hasPermission("flashlight.use.torch.spawn")) {
                if (args.length == 0) {
                    sender.sendMessage(ChatColor.RED + "Usage: /torch spawn");
                }
                if (args.length == 1) {
                    String sub = args[1];
                    if (sub.equals("spawn")) {
                        ItemStack torch = new ItemBuilder(Material.TORCH).withEnchantment(Enchantment.DURABILITY, 1, true)
                                .withName(ChatColor.DARK_AQUA + "Flashlight")
                                .withLore(ChatColor.GREEN + "Use the torch to light your way!")
                                .withAmount(1)
                                .toItemStack();
                        player.getInventory().addItem(torch);
                    }
                }

            } else if (cmd.getName().equalsIgnoreCase("FlashlightReload")) {
                if (sender.hasPermission("flashlight.use.reload")) {
                    plugin.reloadConfig();
                    sender.sendMessage(prefix + ChatColor.GREEN + " Config reloaded");
                    return true;
                }
            }
        }
        return true;
    }
}