package io.FallenYouth.listeners;

import io.FallenYouth.main.FlashlightPlus;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Torch;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Made by FallenYouth
 */

@SuppressWarnings("deprecation")
public class TorchListener implements Listener {
    private FlashlightPlus plugin;

    public TorchListener(FlashlightPlus plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void inHand(PlayerJoinEvent event) {
        Player holder = event.getPlayer();
        ItemStack torch = holder.getItemInHand();

        if (holder.hasPermission("flashlight.use.torch")) {
            if (torch.getItemMeta().getDisplayName().equals(ChatColor.DARK_AQUA + "Flashlight")) {
                if (!plugin.flashlightEnabled.contains(holder.getName()) && holder.getInventory().getItemInHand().getType().equals(Material.TORCH)) {
                    holder.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, true));
                    holder.playEffect(holder.getLocation(), Effect.GHAST_SHOOT, 5);
                    plugin.flashlightEnabled.add(holder.getName());
                } else if (plugin.flashlightEnabled.contains(holder.getName()) && !holder.getInventory().getItemInHand().getType().equals(Material.TORCH)) {
                    holder.removePotionEffect(PotionEffectType.NIGHT_VISION);
                    holder.playEffect(holder.getLocation(), Effect.EXTINGUISH, 5);
                    plugin.flashlightEnabled.remove(holder.getName());
                }
            }
        }
    }
}
